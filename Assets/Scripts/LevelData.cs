﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "Level Data/New Level", order = 1)]
public class LevelData : ScriptableObject
{
    public int cameraFieView;
    public Vector3 CameraPos;
    public float aimRadius;
    public string LevelName;
    public List<PR_Transform> holeList;
    public List<Vector3> ballList;
    public List<PR_Transform> diskList= new List<PR_Transform>();
    public List<Vector3> BombList;
    public List<PR_Transform> CubeList = new List<PR_Transform>();
    public List<FloorTransform> FloorList = new List<FloorTransform>();
    //public List<GTTransform> BlankCubeList = new List<GTTransform>();
    public Vector3 borderSize;
}
[System.Serializable]
public class PR_Transform
{
    public Vector3 position;
    public int index;
    public bool isCorner;
}
