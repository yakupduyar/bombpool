﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    [Header("All Screen Referances")]
    public GameObject PlayingScreen;
    public GameObject WinPanel;
    public GameObject GameOverPanel;
    [Header("playing screen referances")]
    public TextMeshProUGUI currentLevelText;
    public TextMeshProUGUI inSceneBallsText;

    [Header("GameOver screen referances")]
    public TextMeshProUGUI currentLevelTextOver;

    [Header("GameOver screen referances")]
    public TextMeshProUGUI currentLevelTextWin;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void OnClickPlay()
    {
        currentLevelText.text = "LEVEL " + (LevelManager.Instance.CurrentLevelIndex + 1);
        PlayingScreen.SetActive(true);
        WinPanel.SetActive(false);
        GameOverPanel.SetActive(false);
    }

    public void OnGameWin()
    {
        //Debug.LogError(GameManager.Instance.CurrentGameState);
        if (GameManager.Instance.CurrentGameState != GameState.OVER && GameManager.Instance.CurrentGameState != GameState.WIN)
        {
            currentLevelTextWin.text = "LEVEL " + (LevelManager.Instance.CurrentLevelIndex + 1);
            PlayingScreen.SetActive(false);
            WinPanel.SetActive(true);
            GameManager.Instance.CurrentGameState = GameState.WIN;
        }
    }

    public void OnGameOver()
    {
        if (GameManager.Instance.CurrentGameState != GameState.OVER && GameManager.Instance.CurrentGameState != GameState.WIN)
        {
            currentLevelTextOver.text = "LEVEL " + (LevelManager.Instance.CurrentLevelIndex+1);
            PlayingScreen.SetActive(false);
            GameOverPanel.SetActive(true);
            GameManager.Instance.CurrentGameState = GameState.OVER;
        }
    }

    public void OnWinNextLevelClick()
    {
        OnClickPlay();
        LevelManager.Instance.LoadCurrentLevel();
    }

    public void SetUpBallText(int filledBalls,int totalBalls)
    {
        inSceneBallsText.text = filledBalls + " / " + totalBalls;
    }
}
