﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameState { 
START,PLAYING,OVER,WIN
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameState CurrentGameState;
    public GameObject BombPartical;
    public Transform bombParent;


    [Header("Game Settings")]

    public float HitForce;
    public float BallDrag;
    public float BallAngularDrag;
    public float BallMass;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
    }
}
