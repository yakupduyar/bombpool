﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public Rigidbody rb;
    bool isCollideWithHole;
    Transform triggeredHole;
    Vector3 dir;
    float speed;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        rb.mass = GameManager.Instance.BallMass;
        rb.drag = GameManager.Instance.BallDrag;
        rb.angularDrag = GameManager.Instance.BallAngularDrag;
    }

    // Update is called once per frame
    void Update()
    {
        //if (isCollideWithHole)
        //{
        //    if (rb.velocity!=Vector3.zero)
        //    {
        //        rb.velocity = Vector3.zero;
        //    }
        //    this.transform.position = Vector3.MoveTowards(transform.position, triggeredHole.position, 5 * Time.deltaTime);
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Hole")
        {
            rb.mass = 2;
            rb.velocity /= 25;
           
           // rb.angularDrag = 3;
            //Destroy(this.GetComponent<Collider>());
            speed = Mathf.Abs(rb.velocity.magnitude) + 1;
            triggeredHole = other.transform;
            //Debug.LogError("Stop Moving Ball");
            //rb.constraints = RigidbodyConstraints.None;
            //rb.velocity = Vector3.zero;
            //rb.useGravity = false;
            isCollideWithHole = true;
            // Destroy(other);

            //other.gameObject.tag = "Untagged";
            this.gameObject.tag = "Untagged";
            LevelManager.Instance.AddFilledBallCOunt();
        }
    }

    public void DrawSecondLine( Vector3 dir,float length)
    {
        RaycastHit hit2;
        LineRenderer lr2 = this.transform.GetChild(0).GetComponent<LineRenderer>();
        lr2.positionCount = 2;
        lr2.SetPosition(0, this.transform.position);

        if (Physics.Raycast(transform.position, dir, out hit2, length))
        {
            if (hit2.collider.gameObject.tag == "Border")
            {
                lr2.SetPosition(1, hit2.point);
            }
            else
            {
                lr2.SetPosition(1, this.transform.position + dir * length);
             //   Debug.Log(hit2.collider.gameObject.tag);
            }
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit2.distance, Color.yellow);

        }
        else
        {
            lr2.SetPosition(1, this.transform.position + dir * length);
        }
    }
    List<GameObject> CollidedHoles = new List<GameObject>();
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag=="Hole" && !CollidedHoles.Contains(other.transform.parent.gameObject))
        {
            CollidedHoles.Add(other.transform.parent.gameObject);
            Debug.LogError("Hole");
            //rb.mass = 2;
           // rb.velocity/=5;
            //rb.velocity += new Vector3(0, -3, 0);
        }

    }
}
