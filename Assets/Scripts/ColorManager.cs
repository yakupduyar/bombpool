﻿using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public static ColorManager Instance;
    public List<ColorClass> AllColors;
    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
    }
}
[System.Serializable]
public class ColorClass
{
    public Color CameraColor;
    public Color PlateformColor;
    public Color GroundColor;
    public Color DiskColor;
}
