﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float power = 1f;
    public float radius = 5f;
    public float upfource = 1f;
    Rigidbody rb;
    Vector3 dist;
    Vector3 startPos;
    float posX;
    float posY;
    float posZ;
    public Vector3 maxDiskSize;
    List<GameObject> inAreaballs = new List<GameObject>();
    public Transform circle;
    public GameObject psExplode;
    bool isExploding;
    bool isExploded = false;
    public float aimLength;
    //public int CircleSegmentsCount;
    //public LineRenderer lr;

    //private void OnEnable()
    //{
    //    for (int i = 0; i < max; i++)
    //    {

    //    }
    //}
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        power = GameManager.Instance.HitForce;
       


        if (LevelManager.Instance.Hint2)
        {
            transform.Find("CiecleDot").gameObject.SetActive(true);
        }
        else
        {
            transform.Find("CiecleDot").gameObject.SetActive(false);
        }
        this.transform.GetChild(2).gameObject.SetActive(false);
        isExploded = false;
    }
    void CheckForNearestBalls()
    {
        if (!isExploded)
        {
            GameObject[] allBalls = GameObject.FindGameObjectsWithTag("Ball");
            //var pointCount = CircleSegmentsCount + 1; // add extra point to make startpoint and endpoint the same to close the circle
            //var points = new Vector3[pointCount];

            //for (int i = 0; i < pointCount; i++)
            //{
            //    var rad = Mathf.Deg2Rad * (i * 360f / CircleSegmentsCount);
            //    points[i] =this.transform.position + new Vector3(Mathf.Sin(rad) * radius, 0, Mathf.Cos(rad) * radius);
            //    Debug.Log(points[i]);
            //}
            //lr.positionCount = points.Length;
            //lr.SetPositions(points);

            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            List<GameObject> plist = new List<GameObject>();
            foreach (var item in colliders)
            {
                if (item.gameObject.tag == "Ball")
                {
                    if (!inAreaballs.Contains(item.gameObject))
                    {
                        inAreaballs.Add(item.gameObject);
                    }
                    plist.Add(item.gameObject);
                }
            }
            if (plist.Count==0)
            {
                if (LevelManager.Instance.Hint2)
                {
                    for (int i = 0; i < allBalls.Length; i++)
                    {
                        int x = i;
                        allBalls[x].GetComponent<LineRenderer>().positionCount = 0;
                        LineRenderer lr = allBalls[x].transform.GetChild(0).GetComponent<LineRenderer>();
                        lr.positionCount = 2;
                        lr.SetPosition(0, this.transform.position);
                        Vector3 ballPos = allBalls[x].transform.position;// - this.transform.position;
                        Vector3 dir = ((ballPos - this.transform.position));

                        //dir += ballPos;
                        dir.Normalize();
                        //dir += this.transform.position;
                        lr.SetPosition(1, transform.position + (dir * 2));
                    }
                }
                else
                {
                    foreach (GameObject item in allBalls)
                    {
                        item.transform.GetChild(0).GetComponent<LineRenderer>().positionCount = 0;
                        item.GetComponent<LineRenderer>().positionCount = 0;
                    }
                }
            }
            else
            {
                for (int i = 0; i < inAreaballs.Count; i++)
                {
                    if (!plist.Contains(inAreaballs[i]))
                    {
                        LineRenderer lr = inAreaballs[i].GetComponent<LineRenderer>();
                        lr.positionCount = 0;
                        inAreaballs[i].transform.GetChild(0).GetComponent<LineRenderer>().positionCount = 0;
                        inAreaballs.Remove(inAreaballs[i]);
                    }
                }
                foreach (GameObject item in allBalls)
                {
                    if (!inAreaballs.Contains(item))
                    {
                        item.transform.GetChild(0).GetComponent<LineRenderer>().positionCount = 0;
                    }
                }
                foreach (GameObject hit in inAreaballs)
                {
                    if (hit.gameObject.tag == "Ball")
                    {
                        LineRenderer lr = hit.GetComponent<LineRenderer>();
                        lr.positionCount = 2;
                        lr.SetPosition(0, this.transform.position);
                        Vector3 dist = (hit.transform.position - this.transform.position);
                        dist.Normalize();
                        lr.SetPosition(1, hit.transform.position);//+dist*3
                                                                  //LineRenderer lr2 = hit.transform.GetChild(0).GetComponent<LineRenderer>();
                                                                  //lr2.positionCount = 2;
                                                                  //lr2.SetPosition(0, hit.transform.position);


                        //RaycastHit hit2;

                        //if (Physics.Raycast(hit.transform.position, -dist, out hit2, aimLength))
                        //{
                        //    if (hit.gameObject.tag=="Border")
                        //    {
                        //        Debug.Log("Did Hit");
                        //    }
                        //    else
                        //    {
                        //        Debug.Log(hit.gameObject.tag);
                        //    }
                        //    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit2.distance, Color.yellow);

                        //}
                        hit.GetComponent<BallController>().DrawSecondLine(dist, aimLength);
                        //lr2.SetPosition(1, hit.transform.position + dist * aimLength);

                    }
                }
            }
           
        }
       
    }
    private void FixedUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    Detonate();
        //    Debug.Log("bomb fata");
        //    gameObject.SetActive(false);
        //}
        if (isExploding)
        {
            //if (maxDiskSize != new Vector3(radius, 0.002f, radius)) 
            //{
            //    maxDiskSize = new Vector3(radius, 0.002f, radius);
            //}
            circle.localScale = Vector3.MoveTowards(circle.localScale, maxDiskSize, 20 * Time.fixedDeltaTime);
            if (Mathf.Abs(circle.localScale.x-maxDiskSize.x)<0.001f)
            {
                isExploding = false;
                circle.gameObject.SetActive(false);
                Detonate();
            }
            this.transform.GetChild(1).localPosition = Vector3.MoveTowards(transform.GetChild(1).localPosition, new Vector3(0, -0.065f,0), 20 * Time.fixedDeltaTime);
            //if (Mathf.Abs(transform.GetChild(1).localPosition.y+0.065f)<0.03f)
            //{
            //    isExploding = false;
            //  //  Detonate();
            //}
        }
        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;
        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        if (hit.collider != null)
        //        {
        //            if (hit.collider.gameObject==this.gameObject)
        //            {
        //                canDragByUser = true;
        //                startMousePos = GetStartPos();
        //            }
        //            hit.collider.enabled = false;
        //        }
        //    }
        //}
        //if (Input.GetMouseButton(0) && canDragByUser)
        //{
        //    currentMousePos = GetStartPos();
        //    Vector3 diff = currentMousePos - startMousePos;
        //    diff.z = diff.y;
        //    diff.y = 0;
        //    this.transform.position += diff;
        //    startMousePos = currentMousePos;
        //}
//        Debug.LogError("Vishal");
    }
    public void Detonate()
    {
        //foreach (Transform item in GameManager.Instance.bombParent)
        //{
        //    Destroy(item.gameObject);
        //}

        //Vector3 explotionPosition = gameObject.transform.position;
        //colliders = Physics.OverlapSphere(explotionPosition, radius);
        //Instantiate(GameManager.Instance.BombPartical, transform.position, Quaternion.identity,GameManager.Instance.bombParent);
        //foreach (Collider hit in colliders)
        //{
        //    Rigidbody rb = hit.GetComponent<Rigidbody>();
        //    if (rb != null)
        //    {
        //        if (hit.gameObject.tag == "Ball")
        //        {
        //            rb.AddExplosionForce(power, explotionPosition, radius, upfource, ForceMode.Impulse);
        //        }
        //    }
        //}
        //Destroy(this.gameObject);
        //foreach (GameObject hit in inAreaballs)
        //{
        //    if (hit.gameObject.tag == "Ball")
        //    {
        //        LineRenderer lr = hit.GetComponent<LineRenderer>();
        //        lr.positionCount = 0;

        //    }
        //}
        LevelManager.Instance.AddExplodedBomb();
    }

    public void DetonateObject(Collider col)
    {
        if (col.gameObject.GetComponent<Rigidbody>())
        {
            if (this.GetComponent<Collider>()!=null)
            {
                Destroy(this.GetComponent<Collider>());
            }
            Vector3 dir =  col.gameObject.transform.position- this.transform.position ;
            psExplode.SetActive(true);
            //col.GetComponent<Rigidbody>().AddForce(power, transform.position, 10, transform.position.y, ForceMode.Impulse);
            col.GetComponent<Rigidbody>().AddForce(new Vector3(dir.x, col.gameObject.transform.position.y, dir.z) *power, ForceMode.Impulse);
            //col.GetComponent<Rigidbody>().AddRelativeForce(dir * 2000);
        }
    }

    public float Divider = 10;
    public Vector3 GetStartPos()
    {
        //return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y,
            Camera.main.farClipPlane);
        Vector3 mousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y,
           Camera.main.nearClipPlane);
        Vector3 mousePosF = Camera.main.ScreenToWorldPoint(mousePosFar);
        Vector3 mousePosN = Camera.main.ScreenToWorldPoint(mousePosNear);
        return (mousePosF - mousePosN) / Divider;
    }

    void OnMouseDown()
    {
        startPos = transform.position;
        dist = Camera.main.WorldToScreenPoint(transform.position);
        posX = Input.mousePosition.x - dist.x;
        posY = Input.mousePosition.y - dist.y;
        posZ = Input.mousePosition.z - dist.z;
        CheckForNearestBalls();
    }

    void OnMouseDrag()
    {
        CheckForNearestBalls();
        float disX = Input.mousePosition.x - posX;
        float disY = Input.mousePosition.y - posY;
        float disZ = Input.mousePosition.z - posZ;
        Vector3 lastPos = Camera.main.ScreenToWorldPoint(new Vector3(disX, disY, disZ));
        transform.position = new Vector3(lastPos.x, startPos.y, lastPos.z);

    }
  
    private void OnMouseUp()
    {
        if(!isExploded && Mathf.Abs(Vector3.Distance(transform.position,startPos))<0.1f)
        {
            this.transform.GetChild(2).gameObject.SetActive(true);
            psExplode.SetActive(true);
            isExploded = true;
            isExploding = true;
            transform.Find("CiecleDot").gameObject.SetActive(false);
        }
    }
}
