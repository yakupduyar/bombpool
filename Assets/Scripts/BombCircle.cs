﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCircle : MonoBehaviour
{
    List<GameObject> colsTriggered = new List<GameObject>();
    // Start is called before the first frame update

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.tag == "Ball" && !colsTriggered.Contains(other.gameObject))
        //{
        //    colsTriggered.Add(other.gameObject);
        //    Debug.Log("other name " + other.gameObject.name);
        //    this.transform.parent.parent.GetComponent<Bomb>().DetonateObject(other);
        //}
        if (other.gameObject.tag == "Ball" && !colsTriggered.Contains(other.gameObject))
        {
            other.gameObject.GetComponent<LineRenderer>().positionCount = 0;
            other.gameObject.transform.GetChild(0).GetComponent<LineRenderer>().positionCount = 0;
            //Vector3 forceDir = other.transform.position - other.GetContact(0).point;
            // forceDir.Normalize();
            colsTriggered.Add(other.gameObject);
           // Debug.Log("other name " + other.gameObject.name + "  dir  " + forceDir);
            this.transform.parent.parent.GetComponent<Bomb>().DetonateObject(other.gameObject.GetComponent<Collider>());
        }
    }

    //private void OnCollisionEnter(Collision other)
    //{
    //    if (other.gameObject.tag == "Ball" && !colsTriggered.Contains(other.gameObject))
    //    {
    //        other.gameObject.GetComponent<LineRenderer>().positionCount = 0;
    //        other.gameObject.transform.GetChild(0).GetComponent<LineRenderer>().positionCount = 0;
    //        Vector3 forceDir = other.transform.position- other.GetContact(0).point;
    //       // forceDir.Normalize();
    //        colsTriggered.Add(other.gameObject);
    //        Debug.Log("other name " + other.gameObject.name +"  dir  "+forceDir);
    //        this.transform.parent.parent.GetComponent<Bomb>().DetonateObject(other.gameObject.GetComponent<Collider>(),forceDir);
    //    }
    //}
}
