﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[CustomEditor(typeof(LevelGenerater))]
[ExecuteInEditMode]
public class LevelGeneraterEditor : Editor
{
    int index;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelGenerater myTarget = (LevelGenerater)target;
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Reset objects");
        if (GUILayout.Button("Clear level"))
        {
            myTarget.ClearLevel();
        }
        if (GUILayout.Button("Go To Start Position"))
        {
            myTarget.SetCursorToStartPosition();
        }
        if (GUILayout.Button("Delete Object"))
        {
            myTarget.DeleteObject();
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Translation");
        if (GUILayout.Button("Go Left"))
        {
            myTarget.GoLeft();
        }
        if (GUILayout.Button("Go Right"))
        {
            myTarget.GoRight();
        }
        if (GUILayout.Button("Go Up "))
        {
            myTarget.GoUp();
        }
        if (GUILayout.Button("Go Bottom"))
        {
            myTarget.GoDown();
        }
        //-------------------------------------------------------------------

        EditorGUILayout.LabelField("Create floor");
        if (GUILayout.Button("Floor"))
        {
            myTarget.CreateFloor();
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Border objects");
        if (GUILayout.Button("TopLine"))
        {
            index = 1;
            myTarget.CreateNewLine(index);
        }
        if (GUILayout.Button("BottomLine"))
        {
            index = 3;
            myTarget.CreateNewLine(index);
        }
        if (GUILayout.Button("LeftLine"))
        {
            index = 0;
            myTarget.CreateNewLine(index);
        }
        if (GUILayout.Button("RightLine"))
        {
            index = 2;
            myTarget.CreateNewLine(index);
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Corner objects Outside");
        if (GUILayout.Button("CornerOutside Left-Bottom"))
        {
            index = 0;
            myTarget.CreateNewCornerInside(index);
        }
        if (GUILayout.Button("CornerOutside Left-Top"))
        {
            index = 1;
            myTarget.CreateNewCornerInside(index);
        }
        if (GUILayout.Button("CornerOutside Right-Bottom"))
        {
            index = 2;
            myTarget.CreateNewCornerInside(index);
        }
        if (GUILayout.Button("CornerOutside Right-Top"))
        {
            index = 3;
            myTarget.CreateNewCornerInside(index);
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Corner objects Inside");
        if (GUILayout.Button("CornerInside Left-Bottom"))
        {
            index = 4;
            myTarget.CreateNewCornerOutside(index);
        }
        if (GUILayout.Button("CornerInside Left-Top"))
        {
            index = 5;
            myTarget.CreateNewCornerOutside(index);
        }
        if (GUILayout.Button("CornerInside Right-Bottom"))
        {
            index = 6;
            myTarget.CreateNewCornerOutside(index);
        }
        if (GUILayout.Button("CornerInside Right-Top"))
        {
            index = 7;
            myTarget.CreateNewCornerOutside(index);
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Hole objects");
        if (GUILayout.Button("Hole Right"))
        {
            index = 2;
            myTarget.CreateNewHole(index);
        }
        if (GUILayout.Button("Hole Left"))
        {
            index = 0;
            myTarget.CreateNewHole(index);
        }
        if (GUILayout.Button("Hole Bottom"))
        {
            index = 3;
            myTarget.CreateNewHole(index);
        }
        if (GUILayout.Button("Hole Top"))
        {
            index = 1;
            myTarget.CreateNewHole(index);
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Corner Hole objects");
        if (GUILayout.Button("Hole Right Top"))
        {
            index = 4;
            myTarget.CreateNewHole(index);
        }
        if (GUILayout.Button("Hole Left Top"))
        {
            index = 5;
            myTarget.CreateNewHole(index);
        }
        if (GUILayout.Button("Hole Right Bottom"))
        {
            index = 6;
            myTarget.CreateNewHole(index);
        }
        if (GUILayout.Button("Hole Left Bottom"))
        {
            index = 7;
            myTarget.CreateNewHole(index);
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("other all objects");
        if (GUILayout.Button("Generate Ball"))
        {
            myTarget.CreateBall();
        }

        if (GUILayout.Button("Generate Disc"))
        {
            myTarget.CreateDisk();
        }

        if (GUILayout.Button("Generate Bomb"))
        {
            myTarget.CreateBomb();
        }
        //-------------------------------------------------------------------
        EditorGUILayout.LabelField("Save objects");
        if (GUILayout.Button("Save Level"))
        {
            myTarget.SaveLevel();
        }
    }
}

#endif