﻿using UnityEngine;

public class PlateformCreator : MonoBehaviour
{
    public GameObject bottomLine, topLine, rightLine, leftLine;
    public GameObject bottomLeft, bottomRight, topLeft, topRight;
    public GameObject rotateObject;
    public void CreateLine(Vector3 size)
    {
        // clear plateform
        foreach (Transform item in transform)
        {
            Destroy(item.gameObject);
        }

        GameObject go1 = Instantiate(leftLine, this.transform);
        go1.transform.localPosition = new Vector3(-size.x*0.55f, 0, 0);
        go1.transform.localScale = new Vector3(size.z-1f, 1, 1);

        GameObject go2 = Instantiate(rightLine, this.transform);
        go2.transform.localPosition = new Vector3(size.x * 0.55f, 0, 0);
        go2.transform.localScale = new Vector3(size.z-1f, 1, 1);

        GameObject go3 = Instantiate(topLine, this.transform);
        go3.transform.localPosition = new Vector3(0, 0, size.z * 0.55f);
        go3.transform.localScale = new Vector3(size.z-1f, 1, 1);

        GameObject go4 = Instantiate(bottomLine, this.transform);
        go4.transform.localPosition = new Vector3(0, 0, -size.z * 0.55f);
        go4.transform.localScale = new Vector3(size.z-1f, 1, 1);

        GameObject go5 = Instantiate(bottomRight, this.transform);
        go5.transform.localPosition = new Vector3(size.x /2, 0, -size.z / 2);
        go5.transform.localScale = Vector3.one;

        GameObject go6 = Instantiate(bottomLeft, this.transform);
        go6.transform.localPosition = new Vector3(-size.x / 2, 0, -size.z / 2);
        go6.transform.localScale = Vector3.one;

        GameObject go7 = Instantiate(topRight, this.transform);
        go7.transform.localPosition = new Vector3(size.x / 2, 0, size.z / 2);
        go7.transform.localScale = Vector3.one;

        GameObject go8 = Instantiate(topLeft, this.transform);
        go8.transform.localPosition = new Vector3(-size.x / 2, 0, size.z / 2);
        go8.transform.localScale = Vector3.one;
    }

    // Start is called before the first frame update
    void Start()
    {
        CreateLine(new Vector3(10, 0, 10));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
