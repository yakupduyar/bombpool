﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
public class LevelGenerater : MonoBehaviour
{
    [Header("All Variables")]
    public int TotalLvlBall;
    public int cameraFieView;
    public string LevelName = "Level_";
    public Vector3 StartPosition;
    public Transform CurrentCursor;
    public Transform LevelParent;
    public int floorIndex;

    public GameObject CubeObject;
    public GameObject HoleObject;
    public GameObject BallObject;
    public GameObject DiscObject;
    public GameObject BombObject;

    [Header("All Prefabs")]
    public GameObject[] newLine;
    public GameObject[] newCorner;
    public GameObject[] newHoles;
    public GameObject floorNew;
    public GameObject borderCube;

    [Header("All Lists")]
    public List<PR_Transform> CubeList = new List<PR_Transform>();
    public List<FloorTransform> FloorList = new List<FloorTransform>();
    public List<PR_Transform> HoleList = new List<PR_Transform>();
    public List<PR_Transform> DiscList = new List<PR_Transform>();
    public List<Vector3> ballList = new List<Vector3>();
    public List<Vector3> bombList = new List<Vector3>();
    //public List<GTTransform> BlankCubeList = new List<GTTransform>();
    // Start is called before the first frame update

    public void ClearLevel()
    {
        TotalLvlBall = 0;
        cameraFieView = 60;
        LevelName = "LevelN_";
        CubeList = new List<PR_Transform>();
        HoleList = new List<PR_Transform>();
        DiscList = new List<PR_Transform>();
        //BlankCubeList = new List<GTTransform>();
        ballList = new List<Vector3>();
        FloorList = new List<FloorTransform>();
        bombList = new List<Vector3>();
        foreach (Transform item in LevelParent)
        {
            DestroyImmediate(item.gameObject);
        }
    }
    public void DeleteObject()
    {
        Vector3 pos = CurrentCursor.transform.position;
        foreach (var item in CubeList)
        {
            if (item.position==pos)
            {
                CubeList.Remove(item);
                break;
            }
        }
        foreach (var item in HoleList)
        {
            if (item.position == pos)
            {
                HoleList.Remove(item);
                break;
            }
        }
        foreach (var item in DiscList)
        {
            if (item.position == pos)
            {
                DiscList.Remove(item);
                break;
            }
        }
        foreach (var item in ballList)
        {
            if (item == pos)
            {
                ballList.Remove(item);
                break;
            }
        }
        foreach (var item in FloorList)
        {
            if (item.position == pos)
            {
                FloorList.Remove(item);
                break;
            }
        }
        foreach (var item in bombList)
        {
            if (item == pos)
            {
                bombList.Remove(item);
                break;
            }
        }
        foreach (Transform item in this.transform.GetChild(0))
        {
            if (item.transform.position==pos)
            {
                DestroyImmediate(item.gameObject);
            }
        }
    }
    public void SetCursorToStartPosition()
    {
        CurrentCursor.transform.position = StartPosition;
    }
    public void GoLeft()
    {
        CurrentCursor.transform.position += Vector3.left;
    }
    public void GoRight()
    {
        CurrentCursor.transform.position += Vector3.right;
    }
    public void GoUp()
    {
        CurrentCursor.transform.position += Vector3.forward;
    }
    public void GoDown()
    {
        CurrentCursor.transform.position += Vector3.back;
    }

    public void CreatCube()
    {
        PR_Transform g = new PR_Transform();
        g.position = CurrentCursor.transform.position;
        GameObject GO = Instantiate(CubeObject, CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        GO.name = "Cube";
        CubeList.Add(g);
    }
    public void CreateHole()
    {
        PR_Transform g = new PR_Transform();
        g.position = CurrentCursor.transform.position;
        GameObject GO = Instantiate(HoleObject, CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        GO.name = "HOLE";
        HoleList.Add(g);
    }

    public void CreateBall()
    {
        Instantiate(BallObject, CurrentCursor.transform.position, Quaternion.identity, LevelParent);
        ballList.Add(CurrentCursor.transform.position);
    }
    public void CreateBomb()
    {
        Instantiate(BombObject, CurrentCursor.transform.position, Quaternion.identity, LevelParent);
        bombList.Add(CurrentCursor.transform.position);
    }
    public void CreateDisk()
    {
        PR_Transform g = new PR_Transform();
        g.position = CurrentCursor.transform.position;
        Instantiate(DiscObject, CurrentCursor.transform.position, Quaternion.identity, LevelParent);
        DiscList.Add(g);
    }
    public void CreateFloor()
    {
        FloorTransform ft = new FloorTransform();
        ft.position = CurrentCursor.transform.position;
        ft.floorIndex = floorIndex;
        Instantiate(floorNew, CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        FloorList.Add(ft);
    }
    public void CreateNewLine(int index)
    {
        PR_Transform g = new PR_Transform();
        g.index = index;
        g.position = CurrentCursor.transform.position;
        GameObject GO = Instantiate(newLine[index], CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        //GO.name = "Line"+index;
        CubeList.Add(g);
    }
    public void CreateNewHole(int index)
    {
        PR_Transform g = new PR_Transform();
        g.index = index;
        g.position = CurrentCursor.transform.position;
        GameObject GO = Instantiate(newHoles[index], CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        //GO.name = "Hole" + index;
        HoleList.Add(g);
        if (index == 4)
        {
            //right top
            CurrentCursor.transform.position += new Vector3(-0.5f, 0, 0.5f);
        }
        if (index == 5)
        {
            CurrentCursor.transform.position += new Vector3(-0.5f, 0, -0.5f);
        }
        if (index == 6)
        {
            CurrentCursor.transform.position += new Vector3(0.5f, 0, 0.5f);
        }
        if (index == 7)
        {
            CurrentCursor.transform.position += new Vector3(0.5f, 0, -0.5f);
        }
    }
    public void CreateNewCornerInside(int index)
    {
        PR_Transform g = new PR_Transform();
        g.index = index;
        g.isCorner = true;
        g.position = CurrentCursor.transform.position;
        GameObject GO = Instantiate(newCorner[index], CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        //GO.name = "Corner" + index;
        CubeList.Add(g);
    }
    public void CreateNewCornerOutside(int index)
    {
        PR_Transform g = new PR_Transform();
        g.index = index;
        g.isCorner = true;
        g.position = CurrentCursor.transform.position;
        GameObject GO = Instantiate(newCorner[index], CurrentCursor.transform.position, Quaternion.Euler(Vector3.zero), LevelParent);
        //GO.name = "Corner" + index;
        CubeList.Add(g);
       
    }
    public void SaveLevel()
    {
        LevelData asset = ScriptableObject.CreateInstance<LevelData>();
        //asset.TotalLvlBall = TotalLvlBall;
        asset.cameraFieView = 60;
        asset.CubeList = CubeList;
        asset.diskList = DiscList;
        asset.ballList = ballList;
        asset.holeList = HoleList;
        asset.BombList = bombList;
        asset.FloorList = FloorList;
        asset.name = LevelName;
        AssetDatabase.CreateAsset(asset, "Assets/Levels/" + LevelName + ".asset");
        AssetDatabase.SaveAssets();
    }
}
#endif

[System.Serializable]
public class FloorTransform
{
    public Vector3 position;
    public int floorIndex;
}
