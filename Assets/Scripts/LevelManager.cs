﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using TMPro;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public GameObject[] linesObjectsNew;
    public GameObject[] cornersNew;
    public GameObject[] HolesNew;
    public GameObject[] BallsNew;
    public GameObject[] FloorNew;
    int ballIndex;
    public int CurrentLevelIndex
    {
        get
        {
            return PlayerPrefs.GetInt("LevelIndex", 0);
        }
        set
        {
            PlayerPrefs.SetInt("LevelIndex", value);
        }
    }
    public int DummyLevelReached
    {
        get
        {
            return PlayerPrefs.GetInt("DummyLevelReached", 0);
        }
        set
        {
            PlayerPrefs.SetInt("DummyLevelReached", value);
        }
    }

    public bool useLevelIndex;
    public int LevelIndex;
    public LevelData[] AllLevels;
    public LevelData CurrentLevel;
    public GameObject BallPrefab,diskPrefab,bombPrefab,borderPrefab,holePrefab;
   // public TextMeshProUGUI totalBallText, FillBallText;
    public Material normalMaterial;
    public Material borderMaterial;
    public ColorClass CurrentColor;

    [Header("Level ")]
    public Transform LevelParent;


    [Header("Table Details")]
    //public int Column, Row;
    //public Vector3 StartPosition;
    public GameObject[] Ground;

    int inSceneTotalBalls = 0;
    int inSceneFilledBalls = 0;
    int inSceneBomb = 0;
    int inSceneBombBlasted = 0;

    float currentAimLength;

    public bool Hint1;
    public bool Hint2;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
    }
   

    private void Start()
    {
        if (useLevelIndex)
        {
            CurrentLevelIndex = LevelIndex;
        }
        ballIndex = 0;
        SetUpLevelOfIndex(CurrentLevelIndex);
    }

    public void SetUpLevelOfIndex(int levelIndex)
    {
        foreach (Transform item in LevelParent)
        {
            Destroy(item.gameObject);
        }
        
        GameManager.Instance.CurrentGameState = GameState.PLAYING;
        UIManager.Instance.OnClickPlay();

        int indexToLoad = CurrentLevelIndex;
        if (CurrentLevelIndex>=AllLevels.Length)
        {
            indexToLoad = DummyLevelReached;
        }
        CurrentLevel = AllLevels[indexToLoad];
        CurrentColor = ColorManager.Instance.AllColors[Random.Range(0, ColorManager.Instance.AllColors.Count)];
        //for (int i = 0; i < Ground.Length; i++)
        //{
        //    Ground[i].SetActive(true);
        //    //Ground[i].GetComponent<MeshRenderer>().material = normalMaterial;
        //    if (Ground[i].GetComponent<BoxCollider>())
        //    {
        //        Ground[i].tag = "Untagged";
        //        Destroy(Ground[i].GetComponent<BoxCollider>());
        //    }
        //    for (int j = 0; j < CurrentLevel.holeList.Count; j++)
        //    {
        //        //if (Mathf.Abs(i- Mathf.Abs(CurrentLevel.borderSize.z))<0.2f)
        //        //{
        //        //    Ground[i].GetComponent<MeshRenderer>().material = borderMaterial;
        //        //}
        //        if (Ground[i].transform.position == CurrentLevel.holeList[j].position)
        //        {
        //            BoxCollider bc = Ground[i].AddComponent<BoxCollider>();
        //            bc.size = new Vector3(bc.size.x, 2, bc.size.z);
        //            bc.isTrigger = true;
        //            Ground[i].tag = "Hole";
        //            Ground[i].GetComponent<MeshRenderer>().enabled = (false);
        //        }
        //    }
        //}

        //Debug.LogError(CurrentLevelIndex);
        for (int i = 0; i < CurrentLevel.CubeList.Count; i++)
        {
            //if (Mathf.Abs(i- Mathf.Abs(CurrentLevel.borderSize.z))<0.2f)
            //{
            //    Ground[i].GetComponent<MeshRenderer>().material = borderMaterial;
            //}
            //if (Ground[i].transform.position == CurrentLevel.holeList[i].position)
            GameObject go;
            if (CurrentLevel.CubeList[i].isCorner)
            {
                go = Instantiate(cornersNew[CurrentLevel.CubeList[i].index], LevelParent);
            }
            else
            {
                go = Instantiate(linesObjectsNew[CurrentLevel.CubeList[i].index], LevelParent);
            }
            go.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = CurrentColor.PlateformColor;
            go.transform.position = CurrentLevel.CubeList[i].position;
            //go.transform.localEulerAngles = CurrentLevel.CubeList[i].rotation;
        }

        inSceneBomb = CurrentLevel.BombList.Count;
        inSceneBombBlasted = 0;
        inSceneFilledBalls = 0;
        //FillBallText.text = "0";
        inSceneTotalBalls = CurrentLevel.ballList.Count;
        //totalBallText.text = "/ " + CurrentLevel.ballList.Count.ToString();

        UIManager.Instance.SetUpBallText(inSceneFilledBalls,inSceneTotalBalls);
        for (int k = 0; k < CurrentLevel.ballList.Count; k++)
        {
            Instantiate(BallsNew[ballIndex], CurrentLevel.ballList[k], Quaternion.identity, LevelParent);
            ballIndex += 1;
            if (ballIndex>=BallsNew.Length)
            {
                ballIndex = 0;
            }
        }
        for (int k = 0; k < CurrentLevel.holeList.Count; k++)
        {
            GameObject go = Instantiate(HolesNew[CurrentLevel.holeList[k].index], CurrentLevel.holeList[k].position, Quaternion.identity, LevelParent);
            go.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = CurrentColor.PlateformColor;
        }
        for (int l = 0; l < CurrentLevel.diskList.Count; l++)
        {
            Instantiate(diskPrefab, CurrentLevel.diskList[l].position, Quaternion.identity, LevelParent);
        }
        for (int l = 0; l < CurrentLevel.BombList.Count; l++)
        {
            GameObject go = Instantiate(bombPrefab, CurrentLevel.BombList[l], Quaternion.identity, LevelParent);
            go.transform.GetChild(2).GetChild(0).GetComponent<MeshRenderer>().material.color = CurrentColor.DiskColor;
            go.transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().material.color = CurrentColor.DiskColor;
            go.GetComponent<Bomb>().aimLength = CurrentLevel.aimRadius;
        }

        for (int l = 0; l < CurrentLevel.FloorList.Count; l++)
        {
            GameObject go = Instantiate(FloorNew[CurrentLevel.FloorList[l].floorIndex], CurrentLevel.FloorList[l].position, Quaternion.identity, LevelParent);
            go.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = CurrentColor.PlateformColor;
        }

        Camera.main.transform.position = CurrentLevel.CameraPos;
        Camera.main.fieldOfView = CurrentLevel.cameraFieView;
        Camera.main.backgroundColor = CurrentColor.CameraColor;
        //Camera.main.transform.rotation = Quaternion.Euler(CurrentLevel.cameraPosition.rotation);
    }

    public void AddFilledBallCOunt()
    {
        inSceneFilledBalls += 1;
        //FillBallText.text = inSceneFilledBalls.ToString();
        UIManager.Instance.SetUpBallText(inSceneFilledBalls, inSceneTotalBalls);
        if (inSceneFilledBalls==inSceneTotalBalls)
        {
            //Debug.LogError("Game Win");
            UIManager.Instance.OnGameWin();
        }
    }

    public void AddExplodedBomb()
    {
        inSceneBombBlasted += 1;
        if (inSceneBombBlasted == inSceneBomb)
        {
            CheckForGameLoss();
            //Debug.LogError("Game Loss Check");
        }
    }

    void CheckForGameLoss()
    {
        StartCoroutine(CheckLoss());
    }

    IEnumerator CheckLoss()
    {
        yield return new WaitForEndOfFrame();
        int stopCount = 0;
        GameObject[] goBalls = GameObject.FindGameObjectsWithTag("Ball");
        //Debug.LogError(goBalls.Length);
        while(stopCount<=5)
        {
            bool isAllBallStopped = true;
            for (int i = 0; i < goBalls.Length; i++)
            {
                if (GameManager.Instance.CurrentGameState == GameState.WIN)
                {
                    break;
                }
                float f = Mathf.Abs(goBalls[i].GetComponent<Rigidbody>().velocity.sqrMagnitude);
                if (f > 0.2f)
                {
                    isAllBallStopped = false;
                    break;
                }
                if (isAllBallStopped && goBalls.Length == i+1)
                {
                    stopCount += 1;
                }
            }
            if (GameManager.Instance.CurrentGameState == GameState.WIN)
            {
                break;
            }
            else
            {
                yield return new WaitForSeconds(0.1f);
                if (stopCount >= 2 && isAllBallStopped)
                {
                    UIManager.Instance.OnGameOver();
                    Debug.Log("Game Over Here");
                }
            }
        }
    }

    public void LoadNextLevel() 
    {
        UIManager.Instance.WinPanel.SetActive(false);
        UIManager.Instance.PlayingScreen.SetActive(true);
        CurrentLevelIndex++;
        if (CurrentLevelIndex>=AllLevels.Length)
        {
            DummyLevelReached = Random.Range(0, AllLevels.Length);
        }
      //  LevelIndex++;
        Debug.Log("level no ::" + CurrentLevelIndex);
        SetUpLevelOfIndex(CurrentLevelIndex);
    }

    public void LoadCurrentLevel() {
        SetUpLevelOfIndex(CurrentLevelIndex);
    }
}
